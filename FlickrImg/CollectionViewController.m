//
//  CollectionViewController.m
//  FlickrImg
//
//  Created by Sword Software on 30/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"

@interface CollectionViewController (){
    NSMutableArray *imageUrls;
    NSMutableArray *itemIndex;
}

@end


@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    itemIndex = [[NSMutableArray alloc] init];
    NSMutableArray *urlsArray = [[NSMutableArray alloc] init];
    NSString *flickrUrl = [NSString stringWithFormat:@"https://api.flickr.com/services/feeds/photos_public.gne?tags=indiancricket&format=json&nojsoncallback=1"];
    NSError *error;
    NSURLResponse *responses;
    
    NSData * data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:flickrUrl]] returningResponse:&responses error:&error];
    
    if (data) {
        NSData *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSArray *items = [jsonData valueForKey:@"items"];
        
        for (int i = 0; i< items.count; i++) {
            NSData *tmpData = [items objectAtIndex:i];
            NSString *media = [tmpData valueForKey:@"media"];
            NSString *imgUrl = [media valueForKey:@"m"];
            [urlsArray addObject:imgUrl];
        }
    }
    imageUrls = [urlsArray copy];
}


#pragma mark <UICollectionViewDataSource>


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
#warning Incomplete implementation, return the number of sections
    return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of items
    return imageUrls.count;
}





- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.deviceImages.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrls[indexPath.row]]]];
    
    if([itemIndex containsObject:@(indexPath.item)]) {
        [self animateZoomforCell:cell];
        [itemIndex addObject:@(indexPath.item)];
    } else {
        [self animateMinforCell:cell];
        [itemIndex removeObject:@(indexPath.item)];
    }
    
    return cell;
}



#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/





- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    CollectionViewCell *cell2 = [collectionView cellForItemAtIndexPath:indexPath];
    NSInteger itemNumber = indexPath.item;
    
    if([itemIndex containsObject:@(itemNumber)]) {
        [self animateMinforCell:cell2];
        [itemIndex removeObject:@(itemNumber)];
    } else {
        [self animateZoomforCell:cell2];
         [itemIndex addObject:@(itemNumber)];
    }
    
}





-(void)animateZoomforCell:(UICollectionViewCell*)zoomCell
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        zoomCell.transform = CGAffineTransformMakeScale(1.3,1.3);
        
    } completion:^(BOOL finished){
    }];
}

-(void)animateMinforCell:(UICollectionViewCell*)zoomCell
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        zoomCell.transform = CGAffineTransformMakeScale(1,1);
        
        
    } completion:^(BOOL finished){
    }];
}




/*
-(void)animateZoomforCellremove:(UICollectionViewCell*)zoomCell
{
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        zoomCell.transform = CGAffineTransformMakeScale(1.0,1.0);
    } completion:^(BOOL finished){
    }];
}

*/


@end
