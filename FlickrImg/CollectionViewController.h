//
//  CollectionViewController.h
//  FlickrImg
//
//  Created by Sword Software on 30/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionViewController : UICollectionViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@end

NS_ASSUME_NONNULL_END
